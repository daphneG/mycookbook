<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/userDash', 'HomeController@index')->name('home');
Route::get ('/userDash/show_all', 'RecipesController@show_all')->name('recipes.show_all');
Route::post('userDash/recipes/search', 'RecipesController@search')->name('recipes.search');

Route::resource('/userDash/recipes', 'RecipesController');
Route::resource('/userDash/recipes/{recipe}/quantity', 'QuantityController')->except('index');
Route::resource('/userDash/recipes/{recipe}/preparation', 'PreparationController')->except('index');
Route::resource('/userDash/ingredients', 'IngredientsController');


