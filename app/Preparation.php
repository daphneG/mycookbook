<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preparation extends Model
{
    protected $fillable = ['desc'];

    public function recipes(){
        return $this->belongsTo('App\Recipe');
    }
}
