<?php

namespace App\Http\Controllers;

use App\Preparation;
use App\Recipe;
use Auth;
use Illuminate\Http\Request;

class PreparationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( Recipe $recipe)
    {
        return view('recipes.preparation.create', compact('recipe'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request)
    {
        $prep = new Preparation;

        $prep->desc = $request->input('desc');

        $recipe = Auth::user()->recipes()->find($id);
        $recipe->preparations()->save($prep);

        return redirect()->route('recipes.edit', $id)->with('succes', 'Gelukt!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Recipe $recipe, Preparation $preparation)
    {
        return view('recipes.preparation.edit', compact('recipe', 'preparation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Recipe $recipe, Request $request, $id)
    {
        $preparation = Preparation::findOrFail($id);
        $preparation->update($request->only('desc'));

        return redirect()->route('recipes.edit', $recipe->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recipe $recipe, $id)
    {
        $prep = Preparation::findorFail($id);
        $prep->delete();

        return redirect()->route('recipes.edit', $recipe->id);
    }
}
