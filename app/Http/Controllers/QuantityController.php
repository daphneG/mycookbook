<?php

namespace App\Http\Controllers;

use App\Ingredient;
use App\Quantity;
use App\Recipe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\In;

class QuantityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Recipe $recipe)
    {
        $ingredients = Ingredient::all();
        return view('recipes.quantity.create', compact('ingredients', 'recipe'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id ,Request $request)
    {
        $quantity = new Quantity;
        $quantity->ingredient_id = $request->input('ingredient');
        $quantity->quantity = $request->input('quantity');
        $quantity->units = $request->input('units');


        $recipe = Auth::user()->recipes()->find($id);
        $recipe->quantities()->save($quantity);

        return redirect()->route('recipes.edit', $id)->with('success', 'Gelukt!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Recipe $recipe, Quantity $quantity)
    {
        $ingredients = Ingredient::all();
        return view('recipes.quantity.edit', compact( 'recipe', 'quantity', 'ingredients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Recipe $recipe, Request $request, $id)
    {
        $old_quantity = $recipe->quantities()->findOrFail($id);
        $old_quantity->ingredient_id = $request->ingredient;
        $old_quantity->quantity = $request->quantity;
        $old_quantity->units = $request->units;
        $old_quantity->update();

        return redirect()->route('recipes.edit', $recipe->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recipe $recipe, $id)
    {
        $quantity = Quantity::findOrFail($id);
        $quantity->delete();

        return redirect()->route('recipes.edit', $recipe->id);
    }
}
