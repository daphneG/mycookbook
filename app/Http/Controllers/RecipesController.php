<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaveRecipeRequest;
use App\Ingredient;
use App\Preparation;
use App\Quantity;
use App\User;
use App\Recipe;
use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\In;

class RecipesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recipes = Recipe::orderBy('id', 'desc')->paginate(3);
       // $paginate = Recipe::latest();
        return view('recipes.index', compact('recipes', 'paginate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ingredients = Ingredient::all();
        return view('recipes.create', compact('ingredients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveRecipeRequest $request)
    {
        $recipe = new Recipe();
        $photo = new Photo();

        $recipe->name = $request->input('name');

        if ($file = $request->file('file')){

            $name = $file->getClientOriginalName();
            $file->move('images', $name);
            $photo->path = $name;
        }

        $recipe->save();
        $recipe->photos()->save($photo);

        $user = Auth::user();
        $user->recipes()->attach($recipe->id);

        return redirect()->route('recipes.edit', $recipe->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$recipes = Recipe::findOrFail($id);
        $recipe = Auth::user()->recipes->find($id);
        $ingredients = Ingredient::all();
        $preparations = Preparation::all();
        return view('recipes.show', compact( 'recipe', 'ingredients', 'preparations'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $recipe = Recipe::find($id);
        $quantities = $recipe->quantities;
        $preparations = $recipe->preparations;
        $ingredients = Ingredient::all();

        return view('recipes.edit', compact('recipe', 'preparations', 'quantities', 'ingredients' ,'quantity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $recipe = Recipe::findOrFail($id);

        $recipe->update($request->only('name', 'file', 'preparation'));

        return redirect()->route('recipes.show_all');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $recipe = Recipe::find($id);

        foreach ($recipe->quantities as $quantity){
            $quantity->delete();
        }
        foreach($recipe->preparations as $prep){
            $prep->delete();
        }

        $recipe->photos->delete();

        $recipe->delete();
        return redirect()->route('recipes.show_all');
    }

    public function show_all(){

        $recipes = Auth::user()->recipes;
        $ingredients = Ingredient::all();
        $preparations = Preparation::all();


        return view('recipes.show_all', compact('recipes', 'ingredients', 'preparations'));
    }

    public function search(Request $request){

        $search = $request->search;

        $recipes = Recipe::where('name', 'like', '%' . $search . '%')->get();
        $ingredients = Ingredient::all();


        return view('recipes.search', compact('recipes', 'ingredients'));
    }

}
