<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quantity extends Model
{
    protected $fillable = ['quantity', 'units'];

    public function recipes(){
        return $this->belongsTo('App\Recipe');
    }
}
