<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    public $directory = "/images/";

    protected $fillable = ['name'];

    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function photos(){
        return $this->hasOne('App\Photo');
    }

    public function quantities(){
        return $this->hasMany('App\Quantity');
    }

    public function ingredients(){
        return $this->belongsToMany('App\Ingredient');
    }

    public function preparations(){
        return $this->hasMany('App\Preparation');
    }
}
