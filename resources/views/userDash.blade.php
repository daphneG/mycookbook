@extends('layouts.app')

@include('inc.navbarUser')

@section('content')

    @include('inc.sidebar')

    <div class="col col-lg-10">
        <div class="text-center">
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
            </div>
            <div class="alert alert-success">
                <p>You're logged in as Users</p>
            </div>
        </div>
    </div>
@endsection
