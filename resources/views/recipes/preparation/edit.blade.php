@extends('layouts.app')

@include('inc.navbarUser')

@section('content')

    @include('inc.sidebar')

    <div class="col col-lg-10 mt-5">
        <form action="{{ route('preparation.update', [$recipe->id, $preparation->id]) }}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            @method('PUT')
            <div class="col-md-10">
                <div class="card">
                    <h6 class="card-header text-muted">Edit Bereiding</h6>
                    <div class="card-body">
                        <h5 class="card-title">Bereiding</h5>
                        <textarea class="form-control" type="text" name="desc" id="" cols="30" rows="10">{{ $preparation->desc }}</textarea><br>
                        <button class="btn btn-outline-primary btn-sm" type="submit" name="submit">Edit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
