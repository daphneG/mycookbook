@extends('layouts.app')

@include('inc.navbarUser')

@section('content')

    @include('inc.sidebar')

    <div class="col col-lg-10 mt-3">

        <form action="{{ route('recipes.update', $recipe->id) }}" method="post" enctype="multipart/form-data">
            @method ('Put')
            {{ csrf_field() }}
            <div class="col-md-10">
                <div class="card">
                    <h6 class="card-header text-muted">Edit recept</h6>
                    <div class="card-body">
                        <h5 class="card-title">Recepten naam</h5>
                        <input type="text" class="form-control" value="{{ $recipe->name }}"><br>
                        <button class="btn btn-outline-primary btn-sm">Edit recept</button>
                    </div>
                </div>
            </div>
        </form>
        {{--end edit receptnaam--}}

       {{-- Add New Ingredient--}}
        <div class="col col-lg-10">
            <h3>Ingredi&euml;nten</h3>
            <table class="table table-sm table-striped">
                <thead>
                <tr>
                    <th>Ingredient</th>
                    <th>Hoeveelheid</th>
                    <th>Eenheid</th>
                    <th>Controls</th>
                </tr>
                </thead>
                <tbody>
                @foreach($quantities as $quantitie)
                    <tr>

                        <td>{{ $ingredients->where('id',$quantitie->ingredient_id)->first()['name'] }}</td>
                        <td>{{ $quantitie->quantity }}</td>
                        <td>{{ $quantitie->units }}</td>
                        <td>
                            <form action="{{route('quantity.destroy', [$recipe->id ,$quantitie->id])}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="{{ route('quantity.edit' , [$recipe->id, $quantitie->id]) }}" class="btn btn-outline-secondary btn-sm">Edit</a>
                                <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-right">
                <a href="{{ route('quantity.create' , $recipe->id) }}" class="btn btn-sm btn-outline-secondary">Ingredi&euml;nt toevoegen</a>
            </div><br>
        </div>
        {{--    End Add Ingredient--}}

        {{--Add new Preparation--}}
        <div class="col col-lg-10">
            <h3>Bereiding</h3>
                <table class="table table-sm table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Desc</th>
                        <th>Controls</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($preparations as $preparation)
                        <tr>
                            <td>{{ $loop->count }}</td>
                            <td>{{ str_limit(strip_tags($preparation->desc))  }}</td>
                            <td >
                                <form action="{{ route('preparation.destroy', [$recipe->id, $preparation->id]) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <a href="{{ route('preparation.edit', [$recipe->id, $preparation->id]) }}" class="btn btn-outline-secondary btn-sm">Edit</a>
                                    <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            <div class="text-right">
                <a href="{{ route('preparation.create' , $recipe->id) }}" class="btn btn-sm btn-outline-secondary">Bereiding toevoegen</a>
            </div>
        </div>
        {{--    End Preparation--}}

    </div>



@endsection