@extends('layouts.app')

@include('inc.navbarUser')

@section('content')

    @include('inc.sidebar')

    <div class="col col-lg-10 mt-3">
            <form action="{{ route('quantity.update', [$recipe->id, $quantity->id]) }}" method="post">
                {{ csrf_field() }}
                @method('PUT')
                <div class="col-md-10">
                    <div class="card">
                        <h6 class="card-header text-muted">Edit Ingredi&euml;nt</h6>
                        <div class="card-body">
                            <h5 class="card-title">Ingredi&euml;nt</h5>
                            <div class="form-group">
                                <select name="ingredient" id="ingredient" class="form-control">
                                    @foreach($ingredients as $ingredient)
                                        <option value="{{ $ingredient->id }}" @if($ingredient->id === $quantity->ingredient_id) selected @endif>{{ $ingredient->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <h5 class="card-title">Hoeveelheid</h5>
                            <div class="form-group">
                                <input name="quantity" id="quantity" class="form-control" value="{{ $quantity->quantity }}">
                                @if($errors->has('quantity'))
                                    <span class="text-danger">{{ $errors->first('quantity') }}</span>
                                @endif
                            </div>
                            <h5 class="card-title">Eenheden</h5>
                            <div class="form-group">
                                <input name="units" id="units" class="form-control" value="{{ $quantity->units }}">
                                @if($errors->has('units'))
                                    <span class="text-danger">{{ $errors->first('units') }}</span>
                                @endif
                            </div>
                            <button class="btn btn-outline-primary btn-sm" type="submit" name="submit">Edit</button>
                        </div>
                    </div>
                </div>
            </form>
    </div>
@endsection