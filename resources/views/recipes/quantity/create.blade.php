@extends('layouts.app')

@include('inc.navbarUser')

@section('content')

    @include('inc.sidebar')

    <div class="col col-lg-10 mt-3">
            <form action="{{ route('quantity.store' , $recipe->id) }}" method="post">
                {{ csrf_field() }}
                <div class="col-md-10">
                    <div class="card">
                        <h6 class="card-header text-muted">Nieuw Ingredi&euml;nt</h6>
                        <div class="card-body">
                            <h5 class="card-title">Ingredi&euml;nt</h5>
                            <div class="form-group">
                                <select name="ingredient" id="ingredient" class="form-control">
                                    @foreach($ingredients as $ingredient)
                                        <option value="{{ $ingredient->id }}">{{ $ingredient->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <h5 class="card-title">Hoeveelheid</h5>
                            <div class="form-group">
                                <input name="quantity" id="quantity" class="form-control">
                                @if($errors->has('quantity'))
                                    <span class="text-danger">{{ $errors->first('quantity') }}</span>
                                @endif
                            </div>
                            <h5 class="card-title">Eenheden</h5>
                            <div class="form-group">
                                <input name="units" id="units" class="form-control">
                                @if($errors->has('units'))
                                    <span class="text-danger">{{ $errors->first('units') }}</span>
                                @endif
                            </div>
                            <button class="btn btn-outline-primary btn-sm" type="submit" name="submit">Create</button>
                        </div>
                    </div>
                </div>
                {{--<div class="form-group">--}}
                    {{--<label for="ingredient">Ingredi&euml;nt:</label>--}}
                    {{--<select name="ingredient" id="ingredient" class="form-control">--}}
                        {{--@foreach($ingredients as $ingredient)--}}
                        {{--<option value="{{ $ingredient->id }}">{{ $ingredient->name }}</option>--}}
                        {{--@endforeach--}}
                    {{--</select>--}}
                    {{--@if($errors->has('ingredient'))--}}
                        {{--<span class="text-danger">{{ $errors->first('ingredient') }}</span>--}}
                    {{--@endif--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<label for="quantity">Hoeveelheid:</label>--}}
                    {{--<input name="quantity" id="quantity" class="form-control">--}}
                    {{--@if($errors->has('quantity'))--}}
                        {{--<span class="text-danger">{{ $errors->first('quantity') }}</span>--}}
                    {{--@endif--}}
                {{--</div>--}}

                {{--<div class="form-group">--}}
                    {{--<label for="units">Units:</label>--}}
                    {{--<input name="units" id="units" class="form-control">--}}
                    {{--@if($errors->has('units'))--}}
                        {{--<span class="text-danger">{{ $errors->first('units') }}</span>--}}
                    {{--@endif--}}
                {{--</div>--}}

                {{--<div class="form-group">--}}
                    {{--<input class="btn btn-primary" type="submit" name="submit" value="toevoegen">--}}
                {{--</div>--}}
            </form>

    </div>

@endsection