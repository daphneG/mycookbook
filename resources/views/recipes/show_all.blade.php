@extends('layouts.app')

@include('inc.navbarUser')

@section('content')

    @include('inc.sidebar')
    <div class="col col-lg-10 mt-3">
        <h4 class="card-header text-muted">Alle Recepten</h4>

                <table class="table table-sm table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Naam</th>
                        <th>Foto</th>
                        <th>hoeveelheid</th>
                        <th>Unit</th>
                        <th>Ingredienten</th>
                        <th>Bereiding</th>
                        <th width="120">Controls</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($recipes as $recipe)
                    <tr>
                        <td>{{ $recipe->id }}</td>
                        <td>{{ $recipe->name }}</td>
                        <td><img src="/images/{{ $recipe->photos->path }}" width="80px"></td>
                        <td>
                            <ul>
                                @foreach($recipe->quantities as $quantity)
                                    <li>{{$quantity->quantity}}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td>
                            <ul>
                                @foreach($recipe->quantities as $quantity)
                                    <li>{{$quantity->units}}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td>
                            <ul>
                            @foreach($recipe->quantities as $quantity)
                                <li>{{ $ingredients->where('id',$quantity->ingredient_id)->first()['name'] }}</li>
                            @endforeach
                            </ul>
                        </td>
                        <td>
                            @foreach($recipe->preparations as $preparation)
                                {{ str_limit(strip_tags($preparation->desc)) }}
                            @endforeach
                        </td>
                        <td>
                            <form action="{{ route('recipes.destroy', $recipe->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="{{ route('recipes.edit', $recipe->id) }}" class="btn btn-outline-secondary btn-sm">Edit</a>
                                <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                            </form>

                        </td>

                    </tr>
                        @endforeach
                    </tbody>
                </table>
        </h3>
    </div>
    @endsection