@extends('layouts.user')

@section('content')
    <main>
        <div class="col mt-3">
            <div class="card">
                <div class="card-body">
                    <h2 class="card-title">{{ $recipe->name }}</h2>
                    <img src="/images/{{ $recipe->photos->path }}" width="80px" alt="image" class="card-img" style="width: 328px; height: 200px;">
                    <h5 class="mt-3">Ingredi&euml;nten</h5>
                    <ul>
                        @foreach($recipe->quantities as $quantity)
                            <li>{{ $quantity->quantity }} {{$quantity->units}} {{ $ingredients->find($quantity->ingredient_id)->name }}</li>
                        @endforeach

                        
                    </ul>
                    <p class="text"></p>
                    <h5>Bereiding</h5>
                    <p class="card-text">
                        @foreach($recipe->preparations as $preparation)
                            {!! $preparation->desc !!}
                        @endforeach
                    </p>
                </div>
            </div>
            <br>
            <a href="{{ route('recipes.index') }}" class="btn btn-outline-info btn-sm">Back to Overview</a>
        </div>

    </main>
@endsection