@extends('layouts.user')

@section('content')

    <h1>Recepten komen hier</h1>

    <main>
        <div class="col-md-4">
            <div class="well">
                <form action="{{ route('recipes.search') }}" method="post">
                    {{ csrf_field() }}
                    <div class="input-group">
                        <input name="search" type="text" class="form-control" >
                        <span class="form-inline">
                            <button name="submit" class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div><br>
        </div>
        <div class="card-deck">
            @foreach( $recipes as $recipe)
                <div class="col-md-4">
                    <div class="card">
                        <img src="/images/{{ $recipe->photos->path }}" width="80px" alt="image" class="card-img" style="width: 328px; height: 200px;">
                        <div class="card-body">
                            <h5 class="card-title" style="min-height: 50px;">{{ $recipe->name }}</h5>
                            @foreach($recipe->preparations as $preparation)
                                {{ str_limit(strip_tags($preparation->desc)) }}
                            @endforeach
                            <p class="card-text" style="min-height: 30px;">

                            </p>
                            <a href="{{ route('recipes.show', $recipe->id) }}">Lees meer</a>
                        </div>
                    </div>
                    <br>
                </div>
            @endforeach
        </div>
        {{ $recipes->links() }}
    </main>




@endsection