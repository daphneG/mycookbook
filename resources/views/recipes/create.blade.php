@extends('layouts.app')

@include('inc.navbarUser')

@section('content')

    @include('inc.sidebar')

    <div class="col col-lg-10 mt-3">

    @if($errors->any())
        <div class="alert alert-danger mt-5">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
            <form action="{{ route('recipes.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="col-md-10">
                    <div class="card">
                        <h6 class="card-header text-muted">Nieuw recept</h6>
                        <div class="card-body">
                            <h5 class="card-title">Recepten naam</h5>
                            <input type="text" class="form-control" name="name"><br>
                            @if($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                            <div class="form-group">
                                <input type="file" name="file">
                                @if($errors->has('file'))
                                <span class="text-danger">{{ $errors->first('content') }}</span>
                                @endif

                            </div>
                            <button class="btn btn-outline-primary btn-sm" type="submit" name="submit">Create recept</button>
                        </div>


                    </div>
                </div>
                {{--<div class="form-group">--}}
                    {{--<label for="name">Recepten naam</label>--}}
                    {{--<input class="form-control" type="text" name="name">--}}
                    {{--@if($errors->has('name'))--}}
                        {{--<span class="text-danger">{{ $errors->first('name') }}</span>--}}
                    {{--@endif--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<input type="file" name="file">--}}
                    {{--@if($errors->has('file'))--}}
                        {{--<span class="text-danger">{{ $errors->first('content') }}</span>--}}
                    {{--@endif--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<input class="btn btn-primary" type="submit" name="submit" value="Next step">--}}
                {{--</div>--}}
            </form>

    </div>


    @endsection