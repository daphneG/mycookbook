@include('inc.header')

@include('inc.navbarRight')

<div class="container">
    <div class="row">
        @yield('content')
    </div>

</div>

@include('inc.footer')

