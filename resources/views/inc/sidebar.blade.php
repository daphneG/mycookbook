<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" href="/userDash" style="color: #6c757d;">
                    <span class="fas fa-tachometer-alt"></span>
                    Dashboard <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-link" style="color: #6c757d">
                <a href="javascript:;" data-toggle="collapse" data-target="#demo1"  style="color: #6c757d"><span class="far fa-lemon" ></span> Ingredi&euml;nten</a>
                <ul id="demo1" class="collapse">
                    <li>
                        <a href="{{ route('ingredients.index') }}" style="color: #6c757d">Alle Ingredi&euml;nten</a>
                    </li>
                    <li>
                        <a href="{{ route('ingredients.create') }}" style="color: #6c757d">Create</a>
                    </li>
                </ul>
            </li>
            <li class="nav-link">
                <a href="javascript:;" data-toggle="collapse" data-target="#demo" style="color: #6c757d"><span class="fas fa-utensils"></span> Recipes</a>
                <ul id="demo" class="collapse">
                    <li>
                        <a href="{{ route('recipes.show_all') }}" style="color: #6c757d">Alle Recipes</a>
                    </li>
                    <li>
                        <a href="{{ route('recipes.create') }}" style="color: #6c757d">create</a>
                    </li>
                    <li>
                        <a href="{{ route('recipes.index') }}" style="color: #6c757d">Index</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>