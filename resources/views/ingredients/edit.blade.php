@extends('layouts.app')

@include('inc.navbarUser')

@section('content')

    @include('inc.sidebar')

    <div class="col col-lg-10">
        <h3 class="mt-5">Nieuw ingredi&euml;nt</h3>
        <div class="col-md-6">
            <form action="{{ route('ingredients.update', $ingredient->id) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                @method('PUT')
                <div class="col-md-10 mt-5">
                    <div class="card">
                        <h6 class="card-header text-muted">Nieuw Ingredi&euml;nt</h6>
                        <div class="card-body">
                            <h5 class="card-title">Ingredi&euml;nten naam</h5>
                            <input type="text" class="form-control" name="name" value="{{ old('name', $ingredient->name) }}"><br>
                            @if($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                            <button class="btn btn-outline-primary btn-sm" type="submit" name="submit" value="edit ingredient">Edit ingredi&euml;nt</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection