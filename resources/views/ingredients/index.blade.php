@extends('layouts.app')

@include('inc.navbarUser')

@section('content')

    @include('inc.sidebar')
    <div class="col col-lg-10 mt-3">
        {{--Add Ingredient--}}
        <h4 class="card-header text-muted">Ingredi&euml;nten toevoegen</h4>
        <table class="table table-sm table-bordered table-hover">
            <thead>
            <tr>
                <th>Id</th>
                <th>Naam</th>
                <th>Created</th>
                <th>Updated</th>
                <th>Controls</th>
            </tr>
            </thead>
            <tbody>
            @foreach($ingredients as $ingredient)
                <tr>
                    <td>{{ $ingredient->id }}</td>
                    <td>{{ $ingredient->name }}</td>
                    <td>{{ $ingredient->created_at }}</td>
                    <td>{{ $ingredient->updated_at }}</td>
                    <td>
                        <form action="{{ route('ingredients.destroy', $ingredient->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="{{ route('ingredients.edit', $ingredient->id) }}" class="btn btn-outline-secondary btn-sm">Edit</a>
                            <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $ingredients->links() }}

        <div class="form-group">
            <a class="btn btn-outline-info" href="{{ route('ingredients.create') }}">Create Ingredi&euml;nt</a>
        </div>
        {{--End add ingredient--}}
    </div>

@endsection