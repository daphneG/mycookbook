@extends('layouts.app')

@include('inc.navbarUser')

@section('content')

    @include('inc.sidebar')

    <div class="col col-lg-10 mt-3">
            <form action="{{ route('ingredients.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="col-md-10">
                    <div class="card">
                        <h6 class="card-header text-muted">Nieuw Ingredi&euml;nt</h6>
                        <div class="card-body">
                            <h5 class="card-title">Ingredi&euml;nt naam</h5>
                            <div class="form-group">
                                <input class="form-control" type="text" name="name">
                                @if($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                            <button class="btn btn-outline-primary btn-sm" type="submit" name="submit">Create</button>
                        </div>
                    </div>
                </div>
                {{--<div class="form-group">--}}
                    {{--<label for="name">Ingredi&euml;nt naam</label>--}}
                    {{--<input class="form-control" type="text" name="name">--}}
                    {{--@if($errors->has('name'))--}}
                        {{--<span class="text-danger">{{ $errors->first('name') }}</span>--}}
                    {{--@endif--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<input class="btn btn-primary" type="submit" name="submit" value="create">--}}
                {{--</div>--}}
            </form>
    </div>


@endsection