<?php

use App\Ingredient;
use Illuminate\Database\Seeder;

class IngredientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ingredients = new Ingredient;
        $ingredients->name = "appel";
        $ingredients->save();
        unset($ingredients);

        $ingredients = new Ingredient;
        $ingredients->name = "peer";
        $ingredients->save();
        unset($ingredients);

        $ingredients = new Ingredient;
        $ingredients->name = "chocola";
        $ingredients->save();
        unset($ingredients);

    }
}
