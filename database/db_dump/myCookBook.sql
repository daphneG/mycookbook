-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Gegenereerd op: 22 mei 2018 om 19:04
-- Serverversie: 10.1.28-MariaDB
-- PHP-versie: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myCookBook`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ingredients`
--

CREATE TABLE `ingredients` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `ingredients`
--

INSERT INTO `ingredients` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'appel', '2018-05-21 17:27:42', '2018-05-21 17:27:42'),
(2, 'peer', '2018-05-21 17:27:42', '2018-05-21 17:27:42'),
(3, 'chocola', '2018-05-21 17:27:42', '2018-05-21 17:27:42'),
(4, 'volkorenbiscuit', '2018-05-22 10:22:13', '2018-05-22 10:22:13'),
(5, 'boter', '2018-05-22 10:22:20', '2018-05-22 10:22:20'),
(6, 'witte chocolade', '2018-05-22 10:22:28', '2018-05-22 10:22:28'),
(7, 'extra bitter chocolade', '2018-05-22 10:22:34', '2018-05-22 10:22:34'),
(8, 'gelatine blaadjes', '2018-05-22 10:22:43', '2018-05-22 10:22:43'),
(9, 'melk', '2018-05-22 10:22:50', '2018-05-22 10:22:50'),
(10, 'slagroom', '2018-05-22 10:22:58', '2018-05-22 10:22:58'),
(11, 'aardbeien', '2018-05-22 10:23:06', '2018-05-22 10:23:06'),
(12, 'poedersuiker', '2018-05-22 10:23:19', '2018-05-22 10:23:19'),
(13, 'meerzadenbrood', '2018-05-22 10:23:48', '2018-05-22 10:23:48'),
(14, 'ongezouten boter', '2018-05-22 10:24:03', '2018-05-22 10:24:03'),
(15, 'tarwebloem', '2018-05-22 10:24:12', '2018-05-22 10:24:12'),
(16, 'halfvolle melk', '2018-05-22 10:24:20', '2018-05-22 10:24:20'),
(17, 'gemalen nootmuskaat', '2018-05-22 10:24:29', '2018-05-22 10:24:29'),
(18, 'dijonmosterd', '2018-05-22 10:24:38', '2018-05-22 10:24:38'),
(19, 'geraspte gruyere', '2018-05-22 10:24:46', '2018-05-22 10:24:46'),
(20, 'boeren streekham', '2018-05-22 10:24:59', '2018-05-22 10:24:59'),
(21, 'ongebrande ongezouten notenmix', '2018-05-22 10:26:47', '2018-05-22 10:26:47'),
(22, 'sjalotten', '2018-05-22 10:26:53', '2018-05-22 10:26:53'),
(23, 'verse dadels', '2018-05-22 10:27:01', '2018-05-22 10:27:01'),
(24, 'conference peren', '2018-05-22 10:27:09', '2018-05-22 10:27:09'),
(25, 'eieren', '2018-05-22 10:27:16', '2018-05-22 10:27:16'),
(26, 'geitenkaas jong belegen 50+', '2018-05-22 10:27:28', '2018-05-22 10:27:28'),
(27, 'rozemarijn', '2018-05-22 10:27:43', '2018-05-22 10:27:43'),
(28, 'vers bladerdeeg', '2018-05-22 10:27:55', '2018-05-22 10:27:55'),
(29, 'witlof', '2018-05-22 14:42:33', '2018-05-22 14:42:33'),
(30, 'sinaasappel', '2018-05-22 14:42:40', '2018-05-22 14:42:40'),
(31, 'olijfolie extra vierge', '2018-05-22 14:42:56', '2018-05-22 14:42:56'),
(32, 'witte wijnazijn', '2018-05-22 14:43:08', '2018-05-22 14:43:08'),
(33, 'grove mosterd', '2018-05-22 14:43:19', '2018-05-22 14:43:19'),
(34, 'vloeibare honing', '2018-05-22 14:43:30', '2018-05-22 14:43:30'),
(35, 'kipfilet', '2018-05-22 14:48:58', '2018-05-22 14:48:58'),
(36, 'trostomaten', '2018-05-22 14:49:06', '2018-05-22 14:49:06'),
(37, 'baby romainesla', '2018-05-22 14:49:15', '2018-05-22 14:49:15'),
(38, 'milde yoghurt', '2018-05-22 14:49:23', '2018-05-22 14:49:23'),
(39, 'mayonaise', '2018-05-22 14:49:32', '2018-05-22 14:49:32'),
(40, 'kerriepoeder', '2018-05-22 14:49:41', '2018-05-22 14:49:41'),
(41, 'uienpoeder', '2018-05-22 14:49:46', '2018-05-22 14:49:46'),
(42, 'appelciderazijn', '2018-05-22 14:49:56', '2018-05-22 14:49:56'),
(43, 'stevige donkere meergranenbrood', '2018-05-22 14:50:14', '2018-05-22 14:50:14'),
(44, 'limoen', '2018-05-22 14:54:59', '2018-05-22 14:54:59'),
(45, 'zuivelspread', '2018-05-22 14:55:05', '2018-05-22 14:55:05'),
(46, 'verse aardbeien', '2018-05-22 14:55:22', '2018-05-22 14:55:22'),
(47, 'eetrijpe avocado\'s', '2018-05-22 14:55:52', '2018-05-22 14:55:52'),
(48, 'ongezouten cashewnoten', '2018-05-22 14:56:03', '2018-05-22 14:56:03'),
(49, 'eetrijpe mango', '2018-05-22 14:56:11', '2018-05-22 14:56:11'),
(50, 'rode peper', '2018-05-22 14:56:17', '2018-05-22 14:56:17'),
(51, 'verse koriander', '2018-05-22 14:56:27', '2018-05-22 14:56:27'),
(52, 'tortillawraps', '2018-05-22 14:56:35', '2018-05-22 14:56:35');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_05_10_134042_create_photos_table', 1),
(4, '2018_05_10_141744_create_recipes_table', 1),
(5, '2018_05_14_185504_create_recipe_user_table', 1),
(6, '2018_05_15_124248_create_ingredients_table', 1),
(7, '2018_05_15_124259_create_quantities_table', 1),
(8, '2018_05_16_124648_create_preparations_table', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `photos`
--

CREATE TABLE `photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `photos`
--

INSERT INTO `photos` (`id`, `recipe_id`, `path`, `created_at`, `updated_at`) VALUES
(1, 1, 'chocoladetaart5.jpg', '2018-05-22 10:31:45', '2018-05-22 10:31:45'),
(2, 2, 'croque monsieur.jpg', '2018-05-22 11:13:44', '2018-05-22 11:13:44'),
(3, 3, 'quicheGeitenkaas.jpg', '2018-05-22 11:18:21', '2018-05-22 11:18:21'),
(4, 4, 'witlofsalade.jpg', '2018-05-22 14:41:04', '2018-05-22 14:41:04'),
(5, 5, 'clt-sandwich.jpg', '2018-05-22 14:47:44', '2018-05-22 14:47:44'),
(6, 6, 'lunchwrap.jpg', '2018-05-22 14:54:42', '2018-05-22 14:54:42');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `preparations`
--

CREATE TABLE `preparations` (
  `id` int(10) UNSIGNED NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `preparations`
--

INSERT INTO `preparations` (`id`, `recipe_id`, `desc`, `created_at`, `updated_at`) VALUES
(3, 1, '<p>Doe de koekjes in een plastic zakje en rol er met een deegrol over zodat fijne kruimels ontstaan, of verkruimel ze in de foodprocessor.</p><p>Smelt de boter en roer het koek kruimels erdoor. Verdeel het mengsel over de bodem van een met bakpapier beklede springvorm (20 cm) en druk het met de bolle kant van een lepel aan. Laat de bodem in de koelkast opstijven.</p><p>Breek de witte en de pure chocolade in stukjes boven aparte pannetjes. Zet de pannetjes elk in een iets grotere pan met kokend water en laat de chocolade al roerende (au bain-marie) smelten.</p><p>Week de gelatine 5 minuten in koud water. Breng de melk aan de kook en los er, van het vuur af , de goed uitgeknepen gelatine in op. Roer de helft van het gelatine mengsel door de gesmolten witte chocolade en de rest door de gesmolten pure chocolade. Laat beide chocolade mengsels afkoelen, maar niet helemaal opstijven.</p><p>Klop de slagroom stijven spatel de helft door het witte chocolademengsel en de rest door het pure chocolademengsel. Houd 4 eetlepels van de witte mousse apart en doe de rest over in de vorm. Verdeel de bruine mousse erover. Lepel de achter gebleven witte mousse met een theelepeltje over de taart en roer met een vork of satestokje door de bovenste laag van de taart, zodat een marmer effect ontstaat. Laat de mousse in  2-3 uur in de koelkast opstijven.</p><p> Halveer intussen de aardbeien. Haal de spring vormrand voorzichtig los van de taart en verwijder hem. Haal de bodemplaat voorzichtig los, zet de taart op een bord en garneer met de aardbeien. </p><p>Bestuif alleen de aardbeien met poedersuiker. Snijd de taart in punten met een scherp mes.</p>', '2018-05-22 11:07:24', '2018-05-22 11:07:24'),
(4, 2, '<p>Verwarm de oven voor op 200 °C. Snijd de kapjes van het brood. Snijd het brood in 8 sneetjes en rooster ze tijdens het voorverwarmen van de oven ca. 4 min.</p><p>Smelt ondertussen de boter in een steelpan. Voeg de bloem toe. Laat al roerend op laag vuur gaar worden totdat de bloem licht begint te kleuren (roux), dit duurt 5 min. Voeg al roerend de melk toe en breng aan de kook. Voeg de nootmuskaat, peper en eventueel zout toe en laat 5 min. op laag vuur koken tot bechamel.</p><p></p><p>Bestrijk elk sneetje brood aan 1 kant met ½ el mosterd. Leg 4 sneetjes met de mosterdkant naar boven op een met bakpapier beklede bakplaat. Verdeel de helft van de bechamel en de kaas erover. Leg hierop een plak ham en dek af met de rest van de sneetjes brood met de mosterdkant naar onderen. Verdeel de rest van de bechamel erover en bestrooi met de kaas. Bak ca. 10 min. in het midden van de oven.</p>', '2018-05-22 11:17:59', '2018-05-22 11:17:59'),
(5, 3, '<p>Verwarm de oven voor op 180 °C. Hak de noten grof. Verhit een koekenpan zonder olie of boter en rooster de noten 3 min. Snijd ondertussen de sjalotten in dunne ringen. Schep de sjalotten om met de noten en haal van het vuur. Halveer de dadels, verwijder de pit, snijd het vruchtvlees in parten en schep door de noten.</p><p></p><p>Schil de peren en snijd in de lengte in parten. Klop de eieren los met de slagroom. Rasp de geitenkaas. Snijd de naaldjes rozemarijn fijn. Schep de geitenkaas, rozemarijn, peper en eventueel zout door het eiermengsel. Vet de springvorm in.</p><p>Bekleed de springvorm met het bladerdeeg en verdeel het notenmengsel over de bodem van de springvorm. Leg de peer erop en verdeel het eimengsel erover.</p><p>Bak de quiche ca. 55 min. in het midden van de oven. Dek af met aluminiumfolie als hij te donker wordt. Neem uit de oven en laat in de vorm op een rooster in 1 uur afkoelen tot kamertemperatuur.</p><p><strong>Bereidingstip:</strong>Je kunt de quiche 2 dagen van tevoren bereiden. Bewaar afgedekt in de koelkast. Verwarm voor het serveren 30 min. afgedekt met aluminiumfolie in de oven op 170 °C, of serveer hem op kamertemperatuur.</p>', '2018-05-22 11:21:10', '2018-05-22 11:21:33'),
(6, 4, '<p>Snijd de onderkant van de stronken witlof en snijd het witlof doormidden. Verwijder de harde kern. Haal de blaadjes los. Schil met een scherp mes de schil en het wit van de sinaasappel en snijd de vruchtvlees parten uit de vliesjes.</p><p>Klop een dressing van de olie, de azijn, de mosterd en de honing. Breng op smaak met peper en zout.</p><p>Leg de witlofblaadjes en de sinaasappel door elkaar op een schaal. Schenk de dressing erover.</p>', '2018-05-22 14:47:11', '2018-05-22 14:47:11'),
(7, 5, '<p>Breng een pan met een laag water en eventueel wat zout aan de kook. Voeg als het water kookt de kipfilets toe en zet het vuur laag. Pocheer de kip met de deksel op de pan 10 min. Keer halverwege.\r\n</p><p>Snijd ondertussen de tomaten in plakjes en haal de blaadjes van de krop sla los. Neem de gare kip uit het water, laat iets afkoelen en trek met behulp van 2 vorken in plukjes uit elkaar (pulled chicken). Maak een saus van de yoghurt, mayonaise, kerrie- en uienpoeder. Schep ⅔ van de saus samen met de appelcider door de kip. Breng op smaak met peper en eventueel zout.</p><p>Verdeel de kip over de helft van de sneetjes brood en beleg met de tomaat en sla. Besmeer de rest van de sneetjes brood met de rest van de saus en leg met de besmeerde kant op de kip. Snijd schuin door en serveer.</p><p><strong>Variatietip:</strong>\r\nKies eventueel voor wat lichter brood, dat verteert makkelijker.</p><p><strong>Tip:</strong>\r\nDeze sandwich bevat lekker veel eiwitten: deze helpen je spieren te herstellen en te vergroten.</p>', '2018-05-22 14:53:53', '2018-05-22 14:53:53'),
(8, 6, '<p>Rasp de groene schil van de limoen en pers de vrucht uit. Meng de helft van het sap met het rasp, de zuivelspread en de honing. Verwijder de kroontjes van de aardbeien en halveer de vruchten.</p><p> Snijd de avocado’s in de lengte doormidden, verwijder de pit en schep het vruchtvlees met een lepel uit de schil. Snijd het vruchtvlees in blokjes van 1 x 1 cm. Besprenkel met de rest van het limoensap. Hak de cashewnoten grof. Schil de mango en snijd het vruchtvlees in blokjes. Meng met de aardbeien, avocado en cashewnoten.\r\n </p><p> Snijd het steeltje van de rode peper. Verwijder met een scherp mesje de zaadlijsten. Snijd het vruchtvlees fijn. Snijd de koriander grof. Besmeer de tortillawraps met het zuivelspreadmengsel en beleg met de fruit-notensalade. Bestrooi met de koriander en rode peper.\r\n </p>', '2018-05-22 15:00:12', '2018-05-22 15:00:12');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `quantities`
--

CREATE TABLE `quantities` (
  `id` int(10) UNSIGNED NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `ingredient_id` int(11) NOT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `units` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `quantities`
--

INSERT INTO `quantities` (`id`, `recipe_id`, `ingredient_id`, `quantity`, `units`, `created_at`, `updated_at`) VALUES
(2, 1, 5, '75', 'gram', '2018-05-22 10:32:23', '2018-05-22 10:32:23'),
(3, 1, 6, '120', 'gram', '2018-05-22 10:32:38', '2018-05-22 10:32:38'),
(4, 1, 7, '120', 'gram', '2018-05-22 10:32:47', '2018-05-22 10:32:47'),
(5, 1, 8, '4', 'blaadjes', '2018-05-22 10:33:07', '2018-05-22 10:33:07'),
(6, 1, 9, '1', 'dl', '2018-05-22 10:33:21', '2018-05-22 10:33:21'),
(7, 1, 10, '5', 'dl', '2018-05-22 10:33:34', '2018-05-22 11:08:01'),
(8, 1, 11, '4', 'stuks', '2018-05-22 10:33:46', '2018-05-22 10:33:46'),
(9, 1, 12, '1', 'bus', '2018-05-22 10:33:56', '2018-05-22 10:33:56'),
(10, 1, 4, '150', 'gram', '2018-05-22 11:10:25', '2018-05-22 11:10:25'),
(11, 2, 13, '8', 'sneetjes', '2018-05-22 11:15:17', '2018-05-22 11:15:17'),
(12, 2, 14, '25', 'gram', '2018-05-22 11:15:30', '2018-05-22 11:15:30'),
(13, 2, 15, '25', 'gram', '2018-05-22 11:15:48', '2018-05-22 11:15:48'),
(14, 2, 16, '250', 'ml', '2018-05-22 11:16:07', '2018-05-22 11:16:07'),
(15, 2, 18, '4', 'el', '2018-05-22 11:16:19', '2018-05-22 11:16:19'),
(16, 2, 19, '150', 'gram', '2018-05-22 11:16:39', '2018-05-22 11:16:39'),
(17, 2, 20, '4', 'plakken', '2018-05-22 11:16:57', '2018-05-22 11:16:57'),
(18, 2, 17, '1', 'mespunt', '2018-05-22 11:17:17', '2018-05-22 11:17:17'),
(19, 3, 21, '50', 'gram', '2018-05-22 11:18:43', '2018-05-22 11:18:43'),
(20, 3, 22, '3', 'stuks', '2018-05-22 11:18:56', '2018-05-22 11:18:56'),
(21, 3, 23, '125', 'gram', '2018-05-22 11:19:13', '2018-05-22 11:19:13'),
(22, 3, 24, '2', 'stuks', '2018-05-22 11:19:27', '2018-05-22 11:19:27'),
(23, 3, 25, '4', 'stuks', '2018-05-22 11:19:42', '2018-05-22 11:19:42'),
(24, 3, 10, '200', 'ml', '2018-05-22 11:19:56', '2018-05-22 11:19:56'),
(25, 3, 26, '100', 'gram', '2018-05-22 11:20:19', '2018-05-22 11:20:19'),
(26, 3, 27, '2', 'takjes', '2018-05-22 11:20:38', '2018-05-22 11:20:38'),
(27, 3, 28, '1', 'rol (270 gram)', '2018-05-22 11:20:54', '2018-05-22 11:20:54'),
(28, 4, 29, '3', 'stronken', '2018-05-22 14:43:59', '2018-05-22 14:43:59'),
(29, 4, 30, '1', 'stuk', '2018-05-22 14:44:22', '2018-05-22 14:44:22'),
(30, 4, 31, '3', 'el', '2018-05-22 14:44:34', '2018-05-22 14:44:34'),
(31, 4, 32, '1', 'el', '2018-05-22 14:45:09', '2018-05-22 14:45:09'),
(32, 4, 33, '1', 'tl', '2018-05-22 14:45:21', '2018-05-22 14:45:21'),
(33, 4, 34, '1', 'tl', '2018-05-22 14:45:31', '2018-05-22 14:45:31'),
(34, 5, 35, '250', 'gram', '2018-05-22 14:50:30', '2018-05-22 14:50:30'),
(35, 5, 36, '3', 'stuks', '2018-05-22 14:50:42', '2018-05-22 14:50:42'),
(36, 5, 37, '1', 'krop', '2018-05-22 14:50:58', '2018-05-22 14:50:58'),
(37, 5, 38, '50', 'ml', '2018-05-22 14:51:13', '2018-05-22 14:51:13'),
(38, 5, 39, '1', 'el', '2018-05-22 14:51:24', '2018-05-22 14:51:24'),
(39, 5, 40, '1', 'tl', '2018-05-22 14:51:36', '2018-05-22 14:51:36'),
(40, 5, 41, '1/2', 'tl', '2018-05-22 14:51:54', '2018-05-22 14:51:54'),
(41, 5, 42, '1', 'el', '2018-05-22 14:52:04', '2018-05-22 14:52:04'),
(42, 5, 43, '8', 'sneetjes', '2018-05-22 14:52:16', '2018-05-22 14:52:16'),
(43, 6, 44, '1', 'stuk', '2018-05-22 14:56:55', '2018-05-22 14:56:55'),
(44, 6, 45, '200', 'gram', '2018-05-22 14:57:11', '2018-05-22 14:57:11'),
(45, 6, 34, '2', 'el', '2018-05-22 14:57:44', '2018-05-22 14:57:44'),
(46, 6, 46, '250', 'gram', '2018-05-22 14:58:05', '2018-05-22 14:58:05'),
(47, 6, 47, '2', 'stuks', '2018-05-22 14:58:21', '2018-05-22 14:58:21'),
(48, 6, 48, '100', 'gram', '2018-05-22 14:58:35', '2018-05-22 14:58:35'),
(49, 6, 49, '1', 'stuk', '2018-05-22 14:58:48', '2018-05-22 14:58:48'),
(50, 6, 50, '1/2', 'stuk', '2018-05-22 14:59:06', '2018-05-22 14:59:06'),
(51, 6, 51, '15', 'gram', '2018-05-22 14:59:20', '2018-05-22 14:59:20'),
(52, 6, 52, '4', 'stuks', '2018-05-22 14:59:37', '2018-05-22 14:59:37');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `recipes`
--

CREATE TABLE `recipes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `recipes`
--

INSERT INTO `recipes` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Chocolade taart', '2018-05-22 10:31:45', '2018-05-22 10:31:45'),
(2, 'Croque monsieur', '2018-05-22 11:13:44', '2018-05-22 11:13:44'),
(3, 'Quiche met geitenkaas', '2018-05-22 11:18:21', '2018-05-22 11:18:21'),
(4, 'Witlofsalade met sinaasappel', '2018-05-22 14:41:04', '2018-05-22 14:41:04'),
(5, 'CLT-sandwich met pulled chicken', '2018-05-22 14:47:44', '2018-05-22 14:47:44'),
(6, 'Lunchwrap met aardbeien, avocado en mango', '2018-05-22 14:54:42', '2018-05-22 14:54:42');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `recipe_user`
--

CREATE TABLE `recipe_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `recipe_user`
--

INSERT INTO `recipe_user` (`id`, `recipe_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-05-22 10:31:45', '2018-05-22 10:31:45'),
(2, 2, 1, '2018-05-22 11:13:44', '2018-05-22 11:13:44'),
(3, 3, 1, '2018-05-22 11:18:21', '2018-05-22 11:18:21'),
(4, 4, 1, '2018-05-22 14:41:04', '2018-05-22 14:41:04'),
(5, 5, 1, '2018-05-22 14:47:44', '2018-05-22 14:47:44'),
(6, 6, 1, '2018-05-22 14:54:42', '2018-05-22 14:54:42');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Daphne de Groot', 'd.degroot1986@gmail.com', '$2y$10$v7K/Ug.xX1D2NmrwK7AWoehcf9k1jldN4E447luco.0uFu2mH6XKC', NULL, '2018-05-21 17:27:42', '2018-05-21 17:27:42');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `ingredients`
--
ALTER TABLE `ingredients`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexen voor tabel `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `preparations`
--
ALTER TABLE `preparations`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `quantities`
--
ALTER TABLE `quantities`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `recipes`
--
ALTER TABLE `recipes`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `recipe_user`
--
ALTER TABLE `recipe_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `ingredients`
--
ALTER TABLE `ingredients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT voor een tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT voor een tabel `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT voor een tabel `preparations`
--
ALTER TABLE `preparations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT voor een tabel `quantities`
--
ALTER TABLE `quantities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT voor een tabel `recipes`
--
ALTER TABLE `recipes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT voor een tabel `recipe_user`
--
ALTER TABLE `recipe_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
